import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import PropTypes from "prop-types"
import SwipeableViews from "react-swipeable-views"

import { makeStyles, useTheme } from "@material-ui/core/styles"
import AppBar from "@material-ui/core/AppBar"
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab"
import Box from "@material-ui/core/Box"
import Accordion from "@material-ui/core/Accordion"
import AccordionDetails from "@material-ui/core/AccordionDetails"
import AccordionSummary from "@material-ui/core/AccordionSummary"
import Typography from "@material-ui/core/Typography"
import Table from "@material-ui/core/Table"

// Icons - Material UI
import ExpandMoreIcon from "@material-ui/icons/ExpandMore"
import Schedule from "@material-ui/icons/Schedule"
import Star from "@material-ui/icons/Star"
import Book from "@material-ui/icons/Book"

function TabPanel(props) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
}

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  }
}

// Styles
const useStyles = makeStyles(theme => ({
  root: {
    maxWidth: "100%",
    marginTop: "85px",
  },
  details: { maxWidth: "100%", margin: "auto", textAlign: "center" },
  count: { textAlign: "center", color: "darkblue" },
  title: {
    margin: "auto",
    textAlign: "center",
    fontWeight: "bold",
  },
  table: {
    maxWidth: "100%",
  },
  logo: { width: "75px", height: "75px" },
}))

export default function Drink() {
  const data = useStaticQuery(
    graphql`
      {
        allMongodbSkibrewFoodbev(
          filter: {
            category: { drink: { eq: true } }
            town: { eq: "Telluride" }
          }
          sort: { fields: name }
        ) {
          totalCount
          edges {
            node {
              name
              description
              phone
              url
              logo
              how_to_order
              slug
              hours {
                open
                close
              }
              specials {
                daily
              }
            }
          }
        }
      }
    `
  )

  const today = new Date().getDay()

  // State management for Accordion Expansion
  const [expanded, setExpanded] = React.useState(false)

  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false)
  }

  //State management for NavTabs within Accordion Details
  const classes = useStyles()
  const theme = useTheme()
  const [value, setValue] = React.useState(0)

  const handleTabChange = (event, newValue) => {
    setValue(newValue)
  }

  const handleTabChangeIndex = index => {
    setValue(index)
  }

  return (
    <div className={classes.root}>
      <h5 className={classes.count}>
        {data.allMongodbSkibrewFoodbev.totalCount} places to drink in Telluride
      </h5>
      <div>
        {data.allMongodbSkibrewFoodbev.edges.map(({ node }, index) => (
          <Accordion
            expanded={expanded === "panel" + index}
            onChange={handleChange("panel" + index)}
            key={node.id}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panelbh-content"
              id="panelbh-header"
            >
              <img src={node.logo} className={classes.logo} />
              <h5 className={classes.title}>{node.name}</h5>
            </AccordionSummary>
            <AccordionDetails>
              <div className={classes.details}>
                <AppBar position="static" color="default">
                  <SwipeableViews
                    axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                    index={value}
                    onChangeIndex={handleTabChangeIndex}
                  >
                    <TabPanel value={value} index={0} dir={theme.direction}>
                      <Table className="table-striped">
                        <thead>
                          <tr>
                            <th scope="col"> </th>
                            <th scope="col">Open</th>
                            <th scope="col">Close</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">Sunday</th>
                            <td>{node.hours.open[0]}</td>
                            <td>{node.hours.close[0]}</td>
                          </tr>
                          <tr>
                            <th scope="row">Monday</th>
                            <td>{node.hours.open[1]}</td>
                            <td>{node.hours.close[1]}</td>
                          </tr>
                          <tr>
                            <th scope="row">Tuesday</th>
                            <td>{node.hours.open[2]}</td>
                            <td>{node.hours.close[2]}</td>
                          </tr>
                          <tr>
                            <th scope="row">Wednesday</th>
                            <td>{node.hours.open[3]}</td>
                            <td>{node.hours.close[3]}</td>
                          </tr>
                          <tr>
                            <th scope="row">Thursday</th>
                            <td>{node.hours.open[4]}</td>
                            <td>{node.hours.close[4]}</td>
                          </tr>
                          <tr>
                            <th scope="row">Friday</th>
                            <td>{node.hours.open[5]}</td>
                            <td>{node.hours.close[5]}</td>
                          </tr>
                          <tr>
                            <th scope="row">Saturday</th>
                            <td>{node.hours.open[6]}</td>
                            <td>{node.hours.close[6]}</td>
                          </tr>
                        </tbody>
                      </Table>
                    </TabPanel>
                    <TabPanel value={value} index={1} dir={theme.direction}>
                      <Table className="table-striped">
                        <thead>
                          <tr>
                            <th scope="col"> </th>
                            <th scope="col">Special</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">Sunday</th>
                            <td>{node.specials.daily[0]}</td>
                          </tr>
                          <tr>
                            <th scope="row">Monday</th>
                            <td>{node.specials.daily[1]}</td>
                          </tr>
                          <tr>
                            <th scope="row">Tuesday</th>
                            <td>{node.specials.daily[2]}</td>
                          </tr>
                          <tr>
                            <th scope="row">Wednesday</th>
                            <td>{node.specials.daily[3]}</td>
                          </tr>
                          <tr>
                            <th scope="row">Thursday</th>
                            <td>{node.specials.daily[4]}</td>
                          </tr>
                          <tr>
                            <th scope="row">Friday</th>
                            <td>{node.specials.daily[5]}</td>
                          </tr>
                          <tr>
                            <th scope="row">Saturday</th>
                            <td>{node.specials.daily[6]}</td>
                          </tr>
                        </tbody>
                      </Table>
                    </TabPanel>
                    <TabPanel value={value} index={2} dir={theme.direction}>
                      <Table className={"table table-striped"}>
                        <tbody>
                          <tr>
                            <td>{node.description}</td>
                          </tr>
                          <tr>
                            <td>Order: {node.how_to_order}</td>
                          </tr>
                          <tr>
                            <td>
                              <a href={`tel:${node.phone}`}>{node.phone}</a>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <a href={node.url}>{node.name}'s Website</a>
                            </td>
                          </tr>
                        </tbody>
                      </Table>
                    </TabPanel>
                  </SwipeableViews>
                  <Tabs
                    value={value}
                    onChange={handleTabChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="fullWidth"
                    aria-label="full width tabs example"
                  >
                    <Tab label={<Schedule />} {...a11yProps(0)} />
                    <Tab label={<Star />} {...a11yProps(1)} />
                    <Tab label={<Book />} {...a11yProps(2)} />
                  </Tabs>
                </AppBar>
              </div>
            </AccordionDetails>
          </Accordion>
        ))}
      </div>
    </div>
  )
}
