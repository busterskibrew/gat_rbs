import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import PropTypes from "prop-types"
import SwipeableViews from "react-swipeable-views"

import { makeStyles, useTheme } from "@material-ui/core/styles"
import AppBar from "@material-ui/core/AppBar"
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab"
import Box from "@material-ui/core/Box"
import Accordion from "@material-ui/core/Accordion"
import AccordionDetails from "@material-ui/core/AccordionDetails"
import AccordionSummary from "@material-ui/core/AccordionSummary"
import Typography from "@material-ui/core/Typography"
import Table from "@material-ui/core/Table"

// Icons - Material UI
import ExpandMoreIcon from "@material-ui/icons/ExpandMore"
import Schedule from "@material-ui/icons/Schedule"
import Star from "@material-ui/icons/Star"
import Book from "@material-ui/icons/Book"

function TabPanel(props) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
}

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  }
}

// Styles
const useStyles = makeStyles(theme => ({
  root: {
    maxWidth: "100%",
    marginTop: "85px",
  },
  details: { maxWidth: "100%", margin: "auto", textAlign: "center" },
  count: { textAlign: "center", color: "darkblue" },
  title: {
    margin: "auto",
    textAlign: "center",
    fontWeight: "bold",
  },
  table: {
    maxWidth: "100%",
  },
  logo: { width: "75px", height: "75px" },
}))

export default function Shop() {
  const data = useStaticQuery(
    graphql`
      {
        allMongodbSkibrewRetail(filter: { town: { eq: "Telluride" } }) {
          edges {
            node {
              name
              description
            }
          }
          totalCount
        }
      }
    `
  )

  const today = new Date().getDay()

  // State management for Accordion Expansion
  const [expanded, setExpanded] = React.useState(false)

  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false)
  }

  //State management for NavTabs within Accordion Details
  const classes = useStyles()
  const theme = useTheme()
  const [value, setValue] = React.useState(0)

  const handleTabChange = (event, newValue) => {
    setValue(newValue)
  }

  const handleTabChangeIndex = index => {
    setValue(index)
  }

  return (
    <div className={classes.root}>
      <h5 className={classes.count}>
        {data.allMongodbSkibrewRetail.totalCount} places to shop in Telluride
      </h5>
      {/* <div> */}
      {data.allMongodbSkibrewRetail.edges.map(({ node }, index) => (
        <Accordion
          expanded={expanded === "panel" + index}
          onChange={handleChange("panel" + index)}
          key={node.id}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panelbh-content"
            id="panelbh-header"
          >
            <h5 className={classes.title}>{node.name}</h5>
          </AccordionSummary>
          <AccordionDetails>{node.description}</AccordionDetails>
        </Accordion>
      ))}
    </div>
    // </div>
  )
}
