import React from "react"
import { Row, Col, Container } from "react-bootstrap"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout pageInfo={{ pageName: "index" }}>
    <SEO title="Home" keywords={[`gatsby`, `react`, `bootstrap`]} />
    <Container className="text-center">
      <Row>
        <Col>
          <p>Welcome to SkiBrew</p>
        </Col>
      </Row>
      <Row>
        <Col>
          <p>A centralized guide to enhance your mountain town experience.</p>
        </Col>
      </Row>
    </Container>
  </Layout>
)

export default IndexPage
