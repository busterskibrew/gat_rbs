require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
  pathPrefix: "/",
  siteMetadata: {
    title: `SkiBrew`,
    description: `A centralized guide to enhance your mountain town resort experiences. Food, beverage, adventure.`,
    author: `Buster`,
  },
  plugins: [
    {
      // The name of the plugin
      resolve: "gatsby-source-mongodb",
      options: {
        // Name of the database and collection where are data resides
        dbName: "skibrew",
        collection: ["foodbev", "fun", "retail"],
        server: {
          address: process.env.GATSBY_MONGODB_SERVER_ADDRESS,
          port: process.env.GATSBY_MONGODB_SERVER_PORT,
        },
        auth: {
          user: process.env.GATSBY_MONGODB_AUTH_USER,
          password: process.env.GATSBY_MONGODB_AUTH_PW,
        },
        extraParams: {
          replicaSet: "sb-cluster1-shard-0",
          ssl: true,
          authSource: "admin",
          retryWrites: true,
        },
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-plugin-sass`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-react-bootstrap`,
        short_name: `react-bootstrap`,
        start_url: `/`,
        background_color: `#20232a`,
        theme_color: `#20232a`,
        display: `minimal-ui`,
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
